library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.env.all;

library uut_lib;

entity tb_uut is
end entity tb_uut;

architecture RTL of tb_uut is

	signal clk  : std_logic := '0';
	signal ce   : std_logic;
	signal data : std_logic_vector(7 downto 0);
begin

	clk <= not clk after 10 ns;
	
	sim_proc : process
		variable last_data : unsigned(data'range) := (others => '0');
	begin
		ce <= '1';
		wait until rising_edge(clk);
		
		for i in 0 to 2**data'length loop
			wait until rising_edge(clk);
			assert data = std_logic_vector(last_data + 1)
				report "Wrong data!"
				severity error;
			last_data := unsigned(data);
		end loop;
		
		stop;
	end process;
	
	uut : entity uut_lib.uut
		generic map(
			DATA_WIDTH => data'length
		)
		port map(
			clk_in => clk,
			ce_in  => ce,
			data_q => data
		);

end architecture RTL;
