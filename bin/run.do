vlib uut_lib
vlib tb_lib
vmap work tb_lib
vcom -2008 -work uut_lib +cover=bcesxf {src/uut.vhd}
vcom -2008 -work work  {tests/tb_uut.vhd}
vsim -coverage tb_uut
run -all
